def buildJar() {
    echo 'Building The Application'
    sh 'mvn package'
}

def buildAndPushImage() {
    echo 'Building The Docker Image'
    withCredentials([usernamePassword(credentialsId: 'docker-hub-repo', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
        sh 'docker build -t theinstinct/demo-app:jma-2.1 .'
        sh 'echo $PASS | docker login -u $USER --password-stdin'
        sh 'docker push theinstinct/demo-app:jma-2.1'
    }
}

def deployApp() {
    echo 'Deploying Application'
}

return this
