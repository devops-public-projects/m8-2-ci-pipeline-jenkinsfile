
# Creating a CI Pipeline with a Jenkinsfile
In this project, we will create a CI Pipeline with a Jenkinsfile (Freestyle, Pipeline, Multibranch Pipeline).

## Technologies Used
- Jenkins
- Docker
- Git
- Maven
- Node.js
- Java
- DigitalOcean
- Linux (Ubuntu)

## Project Description
CI Pipeline for a Java Maven application to build and push to the repository
- Install Build Tools (Maven, Node) in Jenkins
- Make Docker available on Jenkins server
- Create Jenkins credentials for a git repository
- Create different Jenkins job types (Freestyle, Pipeline, Multibranch Pipeline) for the Java Maven project with Jenkinsfile to:
		- Connect to the application's git repository
		- Build a Jar
		- Build Docker Image
		- Push to private DockerHub repository

## Prerequisites
- Jenkins installed on a remote server as a Docker container
- Docker Hub account
- Nexus Artifact Server

## Guide Steps
### Installing Build Tools
#### Install Maven as a Jenkins Plugin
- Login to Jenkins via your Web browser
- Manage Jenkins > Tools
- Maven > Add Magen
	- Name: maven-3.9
	- Version: 3.9.4 (Latest)
	- **Save**

![Maven Plugin Installed](/images/m8-2-maven-installed-as-plugin.png)

#### Install NPM Directly on Jenkins Server
- `ssh root@JENKINS_SERVER_IP`
- `docker ps`
	- To get the Jenkins CONTAINER_ID
- `docker exec -u 0 -it CONTAINER_ID bash`
	- Enter the container with root user. Not specifying `-u 0` will log us in as jenkins user which doesn't have necessary privileges to install the build tools.
- `cat /etc/issue`
	- To find our OS distribution to look up a proper installation guide for our build tools
- `apt update`
- `apt install curl`
- `curl -sL https://deb.nodesource.com/setup_20.x -o nodesource_setup.sh`
- `bash nodesource_setup.sh`
- `apt install nodejs`
- Verify Install
	- `node -v`
		- Version: 20.5.1
	- `npm -v`
		- Version: 9.8.0

![NPM & NodeJS are now Installed](/images/m8-2-npm-nodejs-installed.png)

#### Install NodeJS Plugin
- Manage Jenkins > Plugins > Available Plugins > Nodejs
- Select Nodejs and **Install without restart**
- Manage Jenkins > Tools
	- NodeJS is now available for configuration and usage in build jobs

![NodeJS Installed as Plugin](/images/m8-2-nodejs-tool-available.png)

#### Configure NodeJS
- **Add NodeJS**
	- Install Automatically is checked
	- Name: my-nodejs
	- Version: Latest Installed
	- **Save**

### Create Freestyle Build Job
- From the Jenkins Dashboard choose **New Item**
- Select **Freestyle Job**
	- Job name: my-job
	- **OK**
- Add Build Steps
	1) Execute Shell
	- `npm --version`
	2) Invoke top-level Maven targets
	- `--version`
- **Save**
- Open the Job from your Dashboard and click **Build Now**

![Build Successful](/images/m8-2-freestyle-build-success.png)
![Build Console Output](/images/m8-2-freestyle-output.png)

### Configure Freestyle Build Job
Note: For this demo we'll use a Freestyle job just for demonstration
- Configure my-job
- Source Code Management > Git
- Repository URL: https://gitlab.com/twn-devops-bootcamp/latest/08-jenkins/java-maven-app.git
- Credentials > Add > Jenkins
	- Kind: **Username with Password**
	- Username: **Your Gitlab email**
	- Password: **Your Gitlab Password**
	- ID: **gitlab-credentials**
	- **Save**
- Credentials > **gitlab-credentials**
- **Save**
- Build Step
	- Execute Shell
	- Commands
		- `chmod +x freestyle_build.sh`
		- `./freestyle_build.sh`

### Run Tests and Build a Java Application
- Dashboard > New Item
	- Name: **java-maven-build**
	- Type: **Freestyle Project**
	- **OK**
	- Repository URL: https://gitlab.com/twn-devops-bootcamp/latest/08-jenkins/java-maven-app.git
	- Credentials: **gitlab-credentials**
	- Branch: ***/jenkins-jobs**
	- Build Step > Add > Invoke top-level Maven targets
		- Maven Version: **Latest**
		- Goals: **test**
	- Build Step > Add > Invoke top-level Maven targets
		- Maven Version: **Latest**
		- Goals: **package**
	- **Save**
	- **Build Now**
- Both Maven Test and Package ran. Package also runs tests which we can see the output for.
- We can also log into the Jenkins server and see the JAR file.

![Maven App Tested and Built Successfully](/images/m8-2-maven-test-build-success.png)

![JAR File](/images/m8-2-jar-file.png)

### Add Docker Into a Jenkins Container
We will mount the Docker directory as a volume in our Jenkins container to make those commands available. Our Jenkins data already persists on our host server so we can make a new container and not lose anything!
- `docker ps`
	- Get the CONTAINER_ID
- `docker stop CONTAINER_ID`
-  Docker run command:
```
docker run -p 8080:8080 -p 50000:50000 -d \
-v jenkins_home:/var/jenkins_home \
-v /var/run/docker.sock:/var/run/docker.sock \
jenkins/jenkins:lts
```
- `docker ps`
	- Get new CONTAINER_ID
- `docker exec -it -u 0 CONTAINER_ID bash`
- `curl [https://get.docker.com/](https://get.docker.com/) > dockerinstall && chmod 777 dockerinstall && ./dockerinstall`
	- This will allow us to run docker commands inside the Jenkins container
- `docker`
	- Shows that you can now run the commands!
- `chmod 666 /var/run/docker.sock`
	- Grant users access (Jenkins user) to run Docker commands

### Build a Docker Image with Jenkins
Using our previous **java-maven-build** freestyle job
- Remove the build step for **maven test**, our **maven package** will run tests for us
- Add a build step for **Execute Shell**
	- `docker build -t java-maven-app .`
		- This will leverage our Dockerfile in the repository from the current directory
- `docker images`
	- Will show our image

![Docker Image Built Successfully](/images/m8-2-docker-image-built.png)

### Push Docker Image To Docker Hub
#### Create Private Repository
We will use a private Docker Repository from Docker
- Log in to Docker Hub
- Create Repository
	- Name: **demo-app**
	- Visibility: **Private**
	- **Create**

#### Configure Docker Hub Credentials in Jenkins
- Dashboard > Manage Jenkins > Security / Credentials > **Add Credentials**
	- Add your Docker Hub credentials
- Dashboard > java-maven-build > **Configure build**
- Build Environment
	- **Use secret text(s) or file(s)**
	- Username Variable: **USERNAME**
	- Password Variable: **PASSWORD**
	- Credentials
		- Newly added Docker Hub Credential

#### Configure Build Step for Docker Push
We will modify the **Execute Shell** steps that previously only contained `docker build -t java-maven-app .`
```
docker build -t DOCKER_REPO_NAME/demo-app:jma-1.0 .
echo $PASSWORD | docker login -u $USERNAME --password-stdin
docker push DOCKER_REPO_NAME/demo-app:jma-1.0
```
- **Build Now**

![Successful Docker Login and Push](/images/m8-2-successful-docker-push.png)

### Push Docker Image To Nexus Repository
As an alternative, we can push the Docker image to Nexus instead of Docker Hub
#### Configure Insecure Registry for Jenkins
- SSH into the server with Jenkins running
- `vim /etc/docker/daemon.json`
```json
{
	"insecure-registries" : ["DROPLET_IP:PORT"]
}	
```
- `systemctl restart docker`
- `docker ps -a`
	- Restarting Docker kills the running containers. Find the previously started container and start it back up.
- `docker start CONTAINER_ID`
- `docker exec -it -u 0 CONTAINER_ID bash`
- `chmod 666 /var/run/docker.sock`
	- Restarting Docker reset the permission for Jenkins to run Docker inside of the container!
#### Configure Nexus Credentials in Jenkins
- Dashboard > Manage Jenkins > Security / Credentials > **Add Credentials**
	- Username with Password
		- Add your Nexus credentials
	- ID: **nexus-docker-repo**
#### Modify Build Steps to Push to Nexus
- Dashboard > Build > Configure
- Modify the **Build Environment** section to use the newly added Nexus credentials instead of the Docker Hub one.
- Modify the Build Steps accordingly
```bash
docker build -t NEXUS_IP:PORT/java-maven-app:1.1 .
echo $PASSWORD | docker login -u $USERNAME --password-stdin NEXUS_IP:PORT
docker push NEXUS_IP:PORT/java-maven-app:1.1
```
- **Save**
- **Build Now**
---
### Configure Full Pipeline w/ Jenkinsfile
We will modify a Jenkinsfile to allow us to utilize Maven to build our JAR file. Then we will build a Docker image, tag it, and upload it to our private Docker repository.

File: **Jenkinsfile_full_pipeline**

- At the top level we will import Maven to be able to use it later
```
tools {
	maven 'maven-3.9'
}
```

- Build JAR Step

`sh 'mvn package'`

- Build Docker Image Step
	- This step will get our configured credentials, build, perform the login, and push to our private Docker repo.
```groovy
withCredentials([usernamePassword(credentialsId: 'docker-hub-repo', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
	sh 'docker build -t DOCKER_REPO/demo-app:jma-2.0 .'
	sh 'echo $PASS | docker login -u $USER --password-stdin'
	sh 'docker push DOCKER_REPO/demo-app:jma-2.0'
}
```
- When we run the build using our new Jenkinsfile, it will update the type of build once the code is checked out again and the changes are acknowledged.
![Modified Jenkins Build](/images/m8-2-full-pipeline-jenkinsfile-build.png)
![Uploaded Docker Image](/images/m8-2-full-pipeline-jenkinsfile-upload.png)

- We can also move all of the `sh` commands to a groovy script to clean up the Jenkinsfile. We can accomplish this by adding a `def gv` at the top of the file and adding a *"init"* stage before we build our JAR file.
```groovy
stage("init") {
	steps {
		script {
			gv = load "script.groovy"
		}
	}	
}
```
- We will also replace the lines in the Jenkinsfile to utilize a `gv.functionCall()`

![Modified Jenkinsfile for Groovy](/images/m8-2-full-pipeline-jenkinsfile-and-groovy-upload.png)

---

### Configure a Multibranch Pipeline
#### Create a Multibranch Pipeline
- Jenkins > New Item
	- Name: **my-multi-branch-pipeline**
	- Type: **Multibranch Pipeline**
	- **OK**

#### Configure Branch Source
- Select **Git**
- Credentials: **Stored Gitlab Credentials**
- Discover Branches: **Filter by Name (with RegEx)**
	- Leave the default as **.***
- **Save**
- A scan will kick off to scan for all branches in that repository

![Multibranch Scan Complete](/images/m8-2-multi-pipeline-scan-complete.png)

![All Branches Found](/images/m8-2-all-branches-found.png)